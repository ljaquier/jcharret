/*----------------------------------------------------------------------------------
   Fichier     : CharretVue.java

   Date        : 13 mai 08

   Auteur      : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But         : Code autogénéré par CloudGarden's Jigloo.
                 Interface graphique du jeu de charret.
                  
   VM          : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package vueClient;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.*;
import client.Client;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/

@SuppressWarnings("serial")
public class CharretVue extends javax.swing.JFrame {

   private JSeparator jSeparator1;
   private JMenuItem terminerMenuItem;
   private JMenuItem aPropos;
   private JMenu jMenu5;
   private ZoneJeu zoneJeu;
   private JMenuItem exitMenuItem;
   private JSeparator jSeparator2;
   private JMenuItem joindreMenuItem;
   private JMenuItem lancerMenuItem;
   private JMenu jMenu3;
   private JMenuBar jMenuBar1;
   
   private Client client;
   
   public CharretVue(Client client)
   {
      super();
      this.client = client;
      initGUI();
      this.setVisible(true);
   }
   
   private void initGUI() {
      try {
         {
            this.setTitle("jCharret");
            this.setResizable(false);
            this.addWindowListener(new WindowAdapter() {
               public void windowClosing(WindowEvent evt) {
                  thisWindowClosing(evt);
               }
            });
         }
         {
            zoneJeu = new ZoneJeu(client);
            getContentPane().add(zoneJeu, BorderLayout.CENTER);
            zoneJeu.setBackground(new java.awt.Color(255,255,255));
         }
         this.setSize(700, 700);
         {
            jMenuBar1 = new JMenuBar();
            setJMenuBar(jMenuBar1);
            {
               jMenu3 = new JMenu();
               jMenuBar1.add(jMenu3);
               jMenu3.setText("Jeu");
               {
                  lancerMenuItem = new JMenuItem();
                  jMenu3.add(lancerMenuItem);
                  lancerMenuItem.setText("Héberger une partie");
                  lancerMenuItem.addActionListener(new ActionListener() {
                     public void actionPerformed(ActionEvent evt) {
                        lancerMenuItemActionPerformed(evt);
                     }
                  });
               }
               {
                  joindreMenuItem = new JMenuItem();
                  jMenu3.add(joindreMenuItem);
                  joindreMenuItem.setText("Joindre une partie");
                  joindreMenuItem.addActionListener(new ActionListener() {
                     public void actionPerformed(ActionEvent evt) {
                        joindreMenuItemActionPerformed(evt);
                     }
                  });
               }
               {
                  jSeparator1 = new JSeparator();
                  jMenu3.add(jSeparator1);
               }
               {
                  terminerMenuItem = new JMenuItem();
                  jMenu3.add(terminerMenuItem);
                  terminerMenuItem.setText("Terminer la partie");
                  terminerMenuItem.addActionListener(new ActionListener() {
                     public void actionPerformed(ActionEvent evt) {
                        terminerMenuItemActionPerformed(evt);
                     }
                  });
                  terminerMenuItem.setEnabled(false);
               }
               {
                  jSeparator2 = new JSeparator();
                  jMenu3.add(jSeparator2);
               }
               {
                  exitMenuItem = new JMenuItem();
                  jMenu3.add(exitMenuItem);
                  exitMenuItem.setText("Quitter");
                  exitMenuItem.addActionListener(new ActionListener() {
                     public void actionPerformed(ActionEvent evt) {
                        exitMenuItemActionPerformed(evt);
                     }
                  });
               }
            }
            {
               jMenu5 = new JMenu();
               jMenuBar1.add(jMenu5);
               jMenu5.setText("?");
               {
                  aPropos = new JMenuItem();
                  jMenu5.add(aPropos);
                  aPropos.setText("À propos");
                  aPropos.addActionListener(new ActionListener() {
                     public void actionPerformed(ActionEvent evt) {
                        aProposActionPerformed(evt);
                     }
                  });
               }
            }
         }
      } catch (Exception e) {
         e.printStackTrace();
      }
   }
   
   /**
    * Lors du clique sur "Quitter"
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param evt
    */ 
   private void exitMenuItemActionPerformed(ActionEvent evt)
   {
      client.terminerPartie();
      this.dispose();
   }
   
   /**
    * Lors de la fermeture de la fenêtre
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param evt
    */ 
   private void thisWindowClosing(WindowEvent evt)
   {
      client.terminerPartie();
      this.dispose();
   }
   
   /**
    * Lors du clique sur "Heberger une partie"
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param evt
    */ 
   private void lancerMenuItemActionPerformed(ActionEvent evt)
   {
      lancerMenuItem.setEnabled(false);
      joindreMenuItem.setEnabled(false);
      terminerMenuItem.setEnabled(true);
      client.lancerPartie();
   }
   
   /**
    * Lors du clique sur "Terminer la partie"
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param evt
    */ 
   private void terminerMenuItemActionPerformed(ActionEvent evt)
   {
      client.terminerPartie();
   }
   
   /**
    * Lors du clique sur "Joindre une partie"
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param evt
    */ 
   private void joindreMenuItemActionPerformed(ActionEvent evt)
   {
      // Demande l'adresse de l'hôte à l'utilisateur
      String hote = JOptionPane.showInputDialog("Enter l'adresse de l'hôte.");
      // On quitte si l'utilisateur a annulé
      if(hote==null) return;
      lancerMenuItem.setEnabled(false);
      joindreMenuItem.setEnabled(false);
      terminerMenuItem.setEnabled(true);
      client.rejoindrePartie(hote);
   }
   
   /**
    * Lors du clique sur "A propos"
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param evt
    */ 
   private void aProposActionPerformed(ActionEvent evt)
   {
      JOptionPane.showMessageDialog(CharretVue.this,
            "jCharret 1.0\n\n" +
            "Auteurs :\n" +
            "Louis Jaquier / Lucien Chaubert / Thierry Forchelet\n\n" +
            "Description :\n" +
            "Jeu du charret en réseau programmé dans le\n" +
            "cadre du mini-projet de GEN à la HEIG-vd.",
            "À propos", JOptionPane.PLAIN_MESSAGE, null);
   }
   
   /**
    * Pour termier la partie au niveau graphique
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   public void terminerPartie()
   {
      zoneJeu.init();
      lancerMenuItem.setEnabled(true);
      joindreMenuItem.setEnabled(true);
      terminerMenuItem.setEnabled(false);
      super.repaint();
   }
   
   /**
    * Pour obtenir la zone de jeu
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public ZoneJeu obtZone()
   {
      return zoneJeu;
   }
   
   /**
    * Afficher une boite de message
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param message
    */ 
   public void msgBox(final String message)
   {
      new Thread()
      {
         /* (non-Javadoc)
          * @see java.lang.Thread#run()
          */
         public void run()
         {
            JOptionPane.showMessageDialog(CharretVue.this, message);
         }
      }.start();
   }
}
