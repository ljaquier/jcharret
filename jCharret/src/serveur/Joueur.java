/*----------------------------------------------------------------------------------
   Fichier     : Joueur.java

   Date        : 13 mai 08

   Auteur      : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But         : Traiter la réception de message et la déconnexion du joueur.
                  
   VM          : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package serveur;

import java.io.*;
import java.net.*;

class Joueur extends Thread
{
   // L'autre joueur
   private Joueur autreJoueur = null;
   // Vartiables pour la connexion
   private Socket socket;
   private PrintWriter canalDeSortie;
   private BufferedReader canalDentree;
   private boolean actif = true;
   // Serveur associé
   private Serveur serveur;

   /**
    * Constructeur de la class Joueur
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param socket
    * @param serveur
    * @throws IOException
    */ 
   public Joueur (Socket socket, Serveur serveur) throws IOException
   {
      this.serveur = serveur;
      this.socket = socket;
      System.out.println ("Serveur : Nouvelle connexion établie");
      canalDeSortie = new PrintWriter(socket.getOutputStream());
      canalDentree = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      this.start();
   }

   /**
    * Corps du thread avec le traitement de la déconnexion du joueur
    * et le renvoye des messages reçu à l'autre joueur.
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   public void run()
   {
      String data;
      try {
         while(actif)
         {
            data = canalDentree.readLine();
            // Si il a y déconnexion
            if(data==null)
               // On arrete la boucle
               actif = false;
            else
               // Sinon on passe le message à l'autre joueur
               if(autreJoueur!=null)
                  autreJoueur.envoyer(data);
         }
      }
      catch (IOException e){}
      // Fermeture des connexions
      fermerConnexion();
      if(autreJoueur!=null)
         autreJoueur.fermerConnexion();
      System.out.println("Serveur : Fermeture d'un serveur d'attente de message");
   }

   /**
    * Pour envoyer un message
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param data
    */ 
   public synchronized void envoyer(String data)
   {
      canalDeSortie.println(data);
      canalDeSortie.flush();
   }
   
   /**
    * Pour définir l'autre joueur
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param autreJoueur
    */ 
   public void defAutreJoueur(Joueur autreJoueur)
   {
      this.autreJoueur = autreJoueur;
   }
   
   /**
    * Pour fermer les connexions
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   public void fermerConnexion()
   {
      // On arrete le Thread
      actif = false;
      // On arret le serveur
      serveur.fermerServeur();
      // On ferme le socket
      if(!socket.isClosed())
         try {
            socket.close();
         } catch (IOException e) {
            System.out.println("Serveur : Erreur : Fermeture de la connexion impossible");
         }
   }
}

