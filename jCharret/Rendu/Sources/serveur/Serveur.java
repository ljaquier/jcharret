/*----------------------------------------------------------------------------------
   Fichier     : Serveur.java

   Date        : 13 mai 08

   Auteur      : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But         : Attendre la connexion des deux joueurs et lancer l'écoute
                 des socket de chaque joueur.
                  
   VM          : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package serveur;

import java.net.*;
import java.io.*;

public class Serveur extends Thread
{
   // Les deux joueurs
   private Joueur[] joueurs = new Joueur[2];
   // Le serveur d'écoute
   private ServerSocket socketServeur;
   // Le port utilisé
   public static final int NO_PORT = 30001;

   /**
    * Constructeur de la class Serveur
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @throws IOException
    */ 
   public Serveur() throws IOException
   {
      socketServeur = new ServerSocket (NO_PORT);
      joueurs[0]=null;
      joueurs[1]=null;
      this.start();
   }

   /**
    * Corps du thread avec l'attente des deux joueurs
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   public void run ()
   {
      System.out.println ("Serveur : En attente de la connexion des joueurs");
      try {
         // Attendre le premier joueur
         joueurs[0]=new Joueur(socketServeur.accept(), this);
         // Attendre le deuxième joueur
         joueurs[1]=new Joueur(socketServeur.accept(), this);
         //
         joueurs[0].defAutreJoueur(joueurs[1]);
         joueurs[1].defAutreJoueur(joueurs[0]);
         // Dit au premier joueur de commencer
         joueurs[0].envoyer("ato#");
      } catch (IOException e) {}
      System.out.println("Serveur : Fermeture du serveur d'attente des joueurs");
   }
   
   /**
    * Pour stopper le serveur
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   public void fermerServeur()
   {
      // On ferme le socket d'écoute
      if(!socketServeur.isClosed())
         try {
            socketServeur.close();
         } catch (IOException e) {
            System.out.println("Serveur : Erreur : Impossible de stopper le serveur");
         }
   }
   
   /**
    * Programme principal pour démarrer uniquement le serveur
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param args
    */ 
   public static void main (String args[])
   {
      try {
         new Serveur();
      } catch (IOException e) {
         System.out.println("Serveur : Erreur : Démarrage du serveur impossible");
      }
   }
}
