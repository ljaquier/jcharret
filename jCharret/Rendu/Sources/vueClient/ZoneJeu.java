/*----------------------------------------------------------------------------------
   Fichier     : ZoneJeu.java

   Date        : 13 mai 08

   Auteur      : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But         : Zone de l'interface graphique où l'on va dessiner.
                  
   VM          : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package vueClient;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import javax.swing.JPanel;
import client.Client;

@SuppressWarnings("serial")
public class ZoneJeu extends JPanel
{
   // Définition de la grille de jeux
   private final static int taille = 500;
   private Pion[][][] pions = new Pion[3][3][3];
   // Données tmp pour le déplacement d'un pion
   private Pion pointTmp = new Pion(-taille, -taille, -1, this);
   private int rTmp, xTmp, yTmp;
   private int deltaXTmp, deltaYTmp;
   private boolean presse = false;
   // Numéro des différents pion
   private static final int pasPion = 0;
   private static final int noJoueur1 = 1;
   private static final int noJoueur2 = 2;
   // Variables pour les actions à effectuer
   private boolean placer = false;
   private boolean deplacer = false;
   private boolean enlever = false;
   // Le client auquel il se rapport
   private Client client;
   // Le message à afficher en-haut de la zone de jeu
   private String message = "";
   
   /**
    * Constructeur de la Zone de jeu
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   public ZoneJeu(Client client)
   {
      super();
      this.client = client;
      init();
      initGUI();
   }
   
   /**
    * Initialise la Zone de jeu
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   private void initGUI()
   {
      try {
         this.setBackground(new java.awt.Color(255,255,255));
         this.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent evt) {
               thisMouseDragged(evt);
            }
         });
         this.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
               thisMouseClicked(evt);
            }
            public void mouseReleased(MouseEvent evt) {
               thisMouseReleased(evt);
            }
            public void mousePressed(MouseEvent evt) {
               thisMousePressed(evt);
            }
         });
      } catch (Exception e) {
         e.printStackTrace();
      }
   }
   
   /**
    * Pour dessiner la zone de jeu
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param g 
    */ 
   public void paintComponent(Graphics g)
   {
      super.paintComponent(g);
      // Dessiner la grille avec les pions
      dessinerGrille(g);
      // Dessiner le pion tmp lors du déplacement
      pointTmp.dessinerPoint(g);
      // Affichage du message en-haut de la zone
      g.setColor(Color.RED);
      g.setFont(new Font("Arial", 0, 20));
      g.drawString(message, 30, 30);
   }
   
   /**
    * Dessine la grille du jeu
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param g Ou dessiner la grille
    */ 
   private void dessinerGrille(Graphics g)
   {
      // Dessine les traits entre les rectangles
      g.setColor(Color.BLACK);
      g.drawLine(getWidth()/2, getHeight()/2-taille/2, getWidth()/2, getHeight()/2-taille/6);
      g.drawLine(getWidth()/2-taille/2, getHeight()/2, getWidth()/2-taille/6, getHeight()/2);
      g.drawLine(getWidth()/2, getHeight()/2+taille/2, getWidth()/2, getHeight()/2+taille/6);
      g.drawLine(getWidth()/2+taille/2, getHeight()/2, getWidth()/2+taille/6, getHeight()/2);
      // Dessine les trois rectangles et les pions
      for(int r=1; r<=3; r++)
      {
         // Dessine un rectangle
         g.setColor(Color.BLACK);
         g.drawRect(getWidth()/2-taille*r/6, getHeight()/2-taille*r/6, taille*r/3, taille*r/3);
         // Dessine les pions sur ce rectangle
         for(int x=1; x<=3; x++)
            for(int y=1; y<=3; y++)
               pions[r-1][x-1][y-1].dessinerPoint(g);
      }
   }
   
   /**
    * Gére le bouton gauche de la souris
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param evt
    */ 
   private void thisMousePressed(MouseEvent evt)
   {
      // Si on presse sur le bouton gauche de la souris et que le joureur ne doit pas prendre
      // un point à l'autre
      if(deplacer && evt.getButton()==MouseEvent.BUTTON1)
         // On trouve le point qui est dessous
         for(int r=1; r<=3; r++)
            for(int x=1;x<=3; x++)
               for(int y=1; y<=3; y++)
                  // Si c'est un de nos pions
                  if(pions[r-1][x-1][y-1].estDessous(evt.getX(), evt.getY()) &&
                     pions[r-1][x-1][y-1].obtNoJoueur()==noJoueur1)
                  {
                     // On crée le point tmp pour le mouvement
                     pointTmp = pions[r-1][x-1][y-1].copie();
                     // On récupère des informations pour le mouvement
                     rTmp = r;
                     xTmp = x;
                     yTmp = y;
                     deltaXTmp = pions[r-1][x-1][y-1].obtX()-evt.getX();
                     deltaYTmp = pions[r-1][x-1][y-1].obtY()-evt.getY();
                     // On remplace de disque courant par un disque neutre
                     pions[r-1][x-1][y-1].defNoJoueur(pasPion);
                     presse = true;
                     super.repaint();
                     return;
                  }
   }
   
   /**
    * Gére le bouton gauche de souris
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param evt
    */ 
   private void thisMouseReleased(MouseEvent evt)
   {
      // Si on relache le bouton gauche de la souris
      if(presse)
      {
         // On trouve le point qui est dessous
         for(int r=1; r<=3; r++)
            for(int x=1;x<=3; x++)
               for(int y=1; y<=3; y++)
                  // Si le déplacement est valide
                  if(pions[r-1][x-1][y-1].estDessous(pointTmp) &&
                     client.deplacerPion(rTmp, xTmp, yTmp, r, x, y))
                  {
                     // On met a jour le point courant
                     pions[r-1][x-1][y-1].defNoJoueur(noJoueur1);
                     // On supprime le point tmp
                     pointTmp = new Pion(-taille, -taille, -1, this);
                     deplacer = false;
                     presse = false;
                     super.repaint();
                     return;
                  }
         // Sinon on remet le plateau dans l'état d'avant
         pions[rTmp-1][xTmp-1][yTmp-1].defNoJoueur(noJoueur1);
         pointTmp = new Pion(-taille, -taille, -1, this);
         presse = false;
         super.repaint();
      }
   }
   
   /**
    * Gère le déplacement de la souris
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param evt
    */ 
   private void thisMouseDragged(MouseEvent evt)
   {
      // Si on déplace la souris avec le bouton gauche pressé
      if(presse)
      {
         // On déplace le point tmp
         pointTmp.defX(evt.getX()+deltaXTmp);
         pointTmp.defY(evt.getY()+deltaYTmp);
         super.repaint();
      }
   }
   
   /**
    * Gère le clique de la souris
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param evt
    */ 
   private void thisMouseClicked(MouseEvent evt)
   {
      if(evt.getButton()==MouseEvent.BUTTON1)
         if(placer)
         {
            // On trouve le point qui est dessous
            for(int r=1; r<=3; r++)
               for(int x=1;x<=3; x++)
                  for(int y=1; y<=3; y++)
                     // Si on peut placer un pion ici
                     if(pions[r-1][x-1][y-1].estDessous(evt.getX(), evt.getY()) &&
                        client.placerPion(r, x, y))
                     {
                        // On place le pion
                        pions[r-1][x-1][y-1].defNoJoueur(noJoueur1);
                        placer = false;
                        super.repaint();
                        return;
                     }
         }
         else if(enlever)
         {
            // On trouve le point qui est dessous
            for(int r=1; r<=3; r++)
               for(int x=1;x<=3; x++)
                  for(int y=1; y<=3; y++)
                     // Si on peut enlever ce pion
                     if(pions[r-1][x-1][y-1].estDessous(evt.getX(), evt.getY()) &&
                        client.enleverPion(r, x, y))
                     {
                        // On remplace de disque courant par un disque neutre
                        pions[r-1][x-1][y-1].defNoJoueur(pasPion);
                        enlever = false;
                        super.repaint();
                        return;
                     }
         }
   }
   
   /**
    * Le joueur doit placer un pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   public void placerPion()
   {
      placer = true;
   }
   
   /**
    * Le joueur doit déplacer un pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   public void deplacerPion()
   {
      deplacer = true;
   }
   
   /**
    * Le joueur doit enlever un pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   public void enleverPion()
   {
      enlever = true;
   }
   
   /**
    * L'autre joueur a place un pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param r
    * @param x
    * @param y
    */ 
   public void placerPionAutre(int r, int x, int y)
   {
      pions[r-1][x-1][y-1].defNoJoueur(noJoueur2);
      super.repaint();
   }
   
   /**
    * L'autre joueur a déplacer un pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param r1
    * @param x1
    * @param y1
    * @param r2
    * @param x2
    * @param y2
    */ 
   public void deplacerPionAutre(int r1, int x1, int y1, int r2, int x2, int y2)
   {
      pions[r2-1][x2-1][y2-1].defNoJoueur(pions[r1-1][x1-1][y1-1].obtNoJoueur());
      pions[r1-1][x1-1][y1-1].defNoJoueur(pasPion);
      super.repaint();
   }
   
   /**
    * L'autre joueur a enlevé un pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param r
    * @param x
    * @param y
    */ 
   public void enleverPionAutre(int r, int x, int y)
   {
      pions[r-1][x-1][y-1].defNoJoueur(pasPion);
      super.repaint();
   }
   
   /**
    * Pour afficher un message en-haut de la zone
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param message
    */ 
   public void afficherMessage(String message)
   {
      this.message = message;
      super.repaint();
   }
   
   /**
    * Initialisation des pions
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   public void init()
   {
      // Calcule les pions
      for(int r=1; r<=3; r++)
      {
         // Ligne du haut
         pions[r-1][0][0] = new Pion(-taille*r/6, -taille*r/6, pasPion, this);
         pions[r-1][1][0] = new Pion(0, -taille*r/6, pasPion, this);
         pions[r-1][2][0] = new Pion(taille*r/6, -taille*r/6, pasPion, this);
         // Ligne du millieu
         pions[r-1][0][1] = new Pion(-taille*r/6, 0, pasPion, this);
         pions[r-1][1][1] = new Pion(-taille, -taille, -1, this);
         pions[r-1][2][1] = new Pion(taille*r/6, 0, pasPion, this);
         // Ligne du bas
         pions[r-1][0][2] = new Pion(-taille*r/6, taille*r/6, pasPion, this);
         pions[r-1][1][2] = new Pion(0, taille*r/6, 0, this);
         pions[r-1][2][2] = new Pion(taille*r/6, taille*r/6, pasPion, this);
      }
   }
}
