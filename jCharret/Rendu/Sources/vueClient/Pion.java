/*----------------------------------------------------------------------------------
   Fichier     : Pion.java

   Date        : 13 mai 08

   Auteur      : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But         : Gérer un pion. (Position, affichage)
                  
   VM          : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package vueClient;

import java.awt.Color;
import java.awt.Graphics;

public class Pion
{
   // Position sur la fenetre
   private int posX;
   private int posY;
   // Caractéristiques selon le numéro du joueur
   private final static int[] diametres = new int[]{0, 20, 40, 40};
   private final static Color[] couleur = new Color[]{Color.WHITE, Color.BLACK, Color.RED, Color.BLUE};
   // La zone de jeu où s'afficher le pion
   private ZoneJeu zone;
   // Numéro du joueur auquel appartient le pion
   private int noJoueur;
   
   /**
    * Constructeur d'un pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param posX
    * @param posY
    * @param diametre
    * @param noJoueur
    */ 
   public Pion(int posX, int posY, int noJoueur, ZoneJeu zone)
   {
      this.posX = posX;
      this.posY = posY;
      this.noJoueur = noJoueur;
      this.zone = zone;
   }
   
   /**
    * Pour dessiner un pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param g
    */ 
   public void dessinerPoint(Graphics g)
   {
      g.setColor(couleur[noJoueur+1]);
      g.fillOval(zone.getWidth()/2+posX-diametres[noJoueur+1]/2, zone.getHeight()/2+posY-diametres[noJoueur+1]/2,
                 diametres[noJoueur+1], diametres[noJoueur+1]);
   }
   
   /**
    * Retourne si le point est dessous un autre défini par des coordonnées
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param x
    * @param y
    * @return
    */ 
   public boolean estDessous(int x, int y)
   {
      return Math.sqrt(Math.pow(zone.getWidth()/2+posX-x,2)+Math.pow(zone.getHeight()/2+posY-y,2))
             <=diametres[noJoueur+1]/2;
   }
   
   /**
    * Retourne si le pion est dessous d'un autre
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param p
    * @return
    */ 
   public boolean estDessous(Pion p)
   {
      return Math.sqrt(Math.pow(zone.getWidth()/2+posX-p.obtX(),2)+Math.pow(zone.getHeight()/2+posY-p.obtY(),2))
             <=diametres[noJoueur+1]+diametres[p.obtNoJoueur()+1]/2;
   }
   
   /**
    * Pour copier un pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public Pion copie()
   {
      return new Pion(posX, posY, noJoueur, zone);
   }
   
   /**
    * Pour définir la position en X du pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param x
    */ 
   public void defX(int x)
   {
      posX = x-zone.getWidth()/2;
   }
   
   /**
    * Pour obtenir la position en X du pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public int obtX()
   {
      return posX+zone.getWidth()/2;
   }
   
   /**
    * Pour définir la position en Y du pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param y
    */ 
   public void defY(int y)
   {
      posY = y-zone.getHeight()/2;
   }
   
   /**
    * Pour obtenir la position en Y du pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public int obtY()
   {
      return posY+zone.getHeight()/2;
   }
   
   /**
    * Pour définir le numéro auquelle appartient le pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param noJoueur
    */ 
   public void defNoJoueur(int noJoueur)
   {
      this.noJoueur = noJoueur;
   }
   
   /**
    * Pour obtenir le numéro auquelle appartient le pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   public int obtNoJoueur()
   {
      return noJoueur;
   }
}
