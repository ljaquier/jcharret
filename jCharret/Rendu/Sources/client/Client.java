/*----------------------------------------------------------------------------------
   Fichier     : Client.java

   Date        : 13 mai 08

   Auteur      : Lucien Chaubert / Louis Jaquier / Thierry Forchelet

   But         : Gérer la partie. Il fait l'interface entre l'autre joueur et
                 l'interface graphique.
                  
   VM          : Java 1.6.0_03
----------------------------------------------------------------------------------*/

package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import serveur.Serveur;
import vueClient.CharretVue;

public class Client
{
   // Information pour la connexion
   private static final int NO_PORT = Serveur.NO_PORT;
   private static final String localhost = "127.0.0.1";
   // Variables pour la connexion
   private Socket socket;
   private BufferedReader canalDentree;
   private PrintWriter canalDeSortie;
   private boolean actif;
   // L'interface graphique conrespondante
   private CharretVue vue;
   // Grille du charret
   private int[][][] pions = new int[3][3][3];
   // Nombre de pion de chaque joueur
   private int nbPion1;
   private int nbPion2;
   // Nombre de pion qui reste à placer par joueur
   private int nbPionAPlacer1;
   private int nbPionAPlacer2;
   // Constantes pour le jeu
   private static final int pasPion = 0;
   private static final int noJoueur1 = 1;
   private static final int noJoueur2 = 2;
   private static final int nbMaxPion = 9;
   // Scanner pour le décodage des informations reçuent
   private Scanner scan;
   
   /**
    * Constructeur du client. La vue est construite en même temps
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   public Client()
   {
      vue = new CharretVue(this);
   }
   
   /**
    * Pour lancer une partie en local
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   public void lancerPartie()
   {
      try {
         vue.obtZone().afficherMessage("Démarrage du serveur...");
         // Démarre le serveur
         new Serveur();
         vue.obtZone().afficherMessage("Serveur démarré");
      } catch (IOException e) {
         vue.obtZone().afficherMessage("Erreur : Démarrage du serveur impossible");
      }
      debutPartie(localhost);
   }
   
   /**
    * Pour rejoindre une partie existante
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param hote
    */ 
   public void rejoindrePartie(String hote)
   {
      // Initialise la partie
      debutPartie(hote);
   }
   
   /**
    * Pour débuter une partie
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   private void debutPartie(String hote)
   {
      // Initialise le plateau du jeu
      for(int r=0; r<3; r++)
      {
         for(int x=0; x<3; x++)
            for(int y=0; y<3; y++)
               pions[r][x][y] = pasPion;
         pions[r][1][1] = -1;
      }
      actif = true;      
      nbPion1 = 0;
      nbPion2 = 0;
      nbPionAPlacer1 = nbMaxPion;
      nbPionAPlacer2 = nbMaxPion;
      
      try {
         // Se connect au serveur
         connexion(hote);
      } catch (UnknownHostException e) {
         vue.obtZone().afficherMessage("Erreur : Adresse de l'hôte invalide");
         this.terminerPartie();
      } catch (IOException e) {
         vue.obtZone().afficherMessage("Erreur : Connexion impossible");
         this.terminerPartie();
      }
   }
   
   /**
    * Pour terminer une partie
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   public void terminerPartie()
   {
      // Stopper le Thread
      actif = false;
      // Fermer le socket
      if(socket!=null && !socket.isClosed())
         try {
            socket.close();
         } catch (IOException e) {
            vue.obtZone().afficherMessage("Erreur : Fermeture du socket impossible");
         }
      vue.terminerPartie();
   }
   
   /**
    * Se connecter au serveur, créer les canaux d'entrée et sortie et 
    * lancer le Thread d'écoute.
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param hote
    * @throws UnknownHostException
    * @throws IOException
    */ 
   private void connexion(String hote) throws UnknownHostException, IOException
   {
      vue.obtZone().afficherMessage("Connexion au serveur...");
      // Connexion au serveur
      socket = new Socket(hote, NO_PORT);
      // Création des canaux d'entrée et sortie
      canalDeSortie = new PrintWriter(socket.getOutputStream());
      canalDentree = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      
      // Création du Thread d'écoute au serveur
      new Thread()
         {
            /* (non-Javadoc)
             * @see java.lang.Thread#run()
             */
            public void run()
            {
               String data;
               try {
                  while(actif)
                  {
                     // Lecture des données reçues
                     data = canalDentree.readLine();
                     // Si la connextion est interompue
                     if(data==null)
                     {
                        vue.obtZone().afficherMessage("L'autre joueur a quitté la partie");
                        terminerPartie();
                     }
                     else
                        // Sinon on traite les données
                        traiterData(data);
                  }
               } catch (IOException e) {}
            }
         }.start();
      vue.obtZone().afficherMessage("Connecté. En attente de l'autre joueur...");
   }
   
   /**
    * Procédure traitant les données reçues par le serveur
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param data
    */ 
   private void traiterData(String data)
   {
      scan = new Scanner(data).useDelimiter("#");
      String prefixe = scan.next();
      // Si c'est a moi
      if(prefixe.equals("ato"))
         traiterAMoi();
      // Si il a placé un pion
      else if(prefixe.equals("pla"))
         traiterPlacer(data);
      // Si il a déplacé un pion
      else if(prefixe.equals("dep"))
         traiterDeplacer(data);
      // Si il a enlevé un pion
      else if(prefixe.equals("enl"))
         traiterEnlever(data);         
   }
   
   /**
    * Traiter l'information que c'est à moi de jouer
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    */ 
   private void traiterAMoi()
   {
      // Si on doit encore placer des pions
      if(nbPionAPlacer1>0)
      {
         vue.obtZone().afficherMessage("Cliquer sur un point noir pour placer un pion");
         // On place un pion
         vue.obtZone().placerPion();
      }
      else
         // Si on peut déplacer un pion
         if(peutDeplacer())
         {
            vue.obtZone().afficherMessage("Deplacer un pion rouge par drag and drop");
            // On déplace un pion
            vue.obtZone().deplacerPion();
         }
         else
         {
            vue.msgBox("Vous ne pouvez pas déplacer de pion");
            vue.obtZone().afficherMessage("L'autre joueur est entrain de jouer...");
            // On dit au serveur qu'on a fini
            envoyer("ato#");
         }
   }
   
   /**
    * Traiter l'information que l'autre joueur à placer un pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param data
    */ 
   private void traiterPlacer(String data)
   {
      // On extrait les informations
      int r = scan.nextInt();
      int x = scan.nextInt();
      int y = scan.nextInt();
      // On place le pion
      pions[r-1][x-1][y-1] = noJoueur2;
      vue.obtZone().placerPionAutre(r, x, y);
      nbPionAPlacer2--;
      nbPion2++;
   }
   
   /**
    * Traiter l'information que l'autre joueur à déplacer un pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param data
    */ 
   private void traiterDeplacer(String data)
   {
      // On extrait les informations
      int r1 = scan.nextInt();
      int x1 = scan.nextInt();
      int y1 = scan.nextInt();
      int r2 = scan.nextInt();
      int x2 = scan.nextInt();
      int y2 = scan.nextInt();
      // On déplace le pion
      pions[r2-1][x2-1][y2-1] = pions[r1-1][x1-1][y1-1];
      pions[r1-1][x1-1][y1-1] = pasPion;
      vue.obtZone().deplacerPionAutre(r1, x1, y1, r2, x2, y2);
   }
   
   /**
    * Traiter l'information que l'autre joueur à enlever un pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param data
    */ 
   private void traiterEnlever(String data)
   {
      // On extrait les informations
      int r = scan.nextInt();
      int x = scan.nextInt();
      int y = scan.nextInt();
      // On enlève le pion
      pions[r-1][x-1][y-1] = pasPion;
      vue.obtZone().enleverPionAutre(r, x, y);
      nbPion1--;
      
      // Si on a perdu
      if(perdu())
      {
         vue.obtZone().afficherMessage("");
         vue.msgBox("Vous avez perdu");
      }
   }
   
   /**
    * Envoyer des données au serveur
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param data
    */ 
   private synchronized void envoyer(String data)
   {
      canalDeSortie.println(data);
      canalDeSortie.flush();
   }
   
   /**
    * Pour déplacer un pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param r1
    * @param x1
    * @param y1
    * @param r2
    * @param x2
    * @param y2
    * @return Si le déplacement est valide
    */ 
   public boolean deplacerPion(int r1, int x1, int y1, int r2, int x2, int y2)
   {
      // Si on peux déplacer le pion
      if(deplacementValide(r1, x1, y1, r2, x2, y2))
      {
         // On déplace le pion
         pions[r2-1][x2-1][y2-1] = pions[r1-1][x1-1][y1-1];
         pions[r1-1][x1-1][y1-1] = pasPion;
         // On dit au serveur qu'on a déplacer un pion
         envoyer("dep#"+r1+"#"+x1+"#"+y1+"#"+r2+"#"+x2+"#"+y2+"#");
         testCharret(r2, x2, y2);
         return true;
      }
      return false;
   }
   
   /**
    * Test si un déplacement est valide
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param r1
    * @param x1
    * @param y1
    * @param r2
    * @param x2
    * @param y2
    * @return
    */ 
   private boolean deplacementValide(int r1, int x1, int y1, int r2, int x2, int y2)
   {
      return pions[r1-1][x1-1][y1-1]==noJoueur1 &&
             pions[r2-1][x2-1][y2-1]==pasPion &&
             (nbPion1<=3 ||
              ((r1==r2 && ((x1==x2 && Math.abs(y1-y2)==1) ||
                           (y1==y2 && Math.abs(x1-x2)==1))) ||
               (x1==x2 && y1==y2 && (x1==2 || y1==2) && Math.abs(r1-r2)==1)));
   }
   
   /**
    * Pour savoir si le joueur peut déplacer un pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   private boolean peutDeplacer()
   {
      for(int r1=1; r1<=3; r1++)
         for(int x1=1; x1<=3; x1++)
            for(int y1=1; y1<=3; y1++)
               for(int r2=1; r2<=3; r2++)
                  for(int x2=1; x2<=3; x2++)
                     for(int y2=1; y2<=3; y2++)
                        if(deplacementValide(r1, x1, y1, r2, x2, y2))
                           return true;
      return false;
   }
   
   /**
    * Test si il y a un charret et le traite le cas échéant
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param r
    * @param x
    * @param y
    */ 
   private void testCharret(int r, int x, int y)
   {
      // Si on a fait un charret
      if(charret(r, x, y))
      {
         if(peutEnlever())
         {
            vue.obtZone().enleverPion();
            vue.obtZone().afficherMessage("Cliquer sur un pion bleu pour le prendre");
            return;
         }
         else
            vue.msgBox("Vous avez effectué un charret mais \n" +
                       "ne pouvez pas enlever un pion à l'autre joueur");
      }
      vue.obtZone().afficherMessage("L'autre joueur est entraint de jouer...");
      // On dit au serveur qu'on a fini
      envoyer("ato#");
   }
   
   /**
    * Pour placer un pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param r
    * @param x
    * @param y
    * @return Si le placement est valide
    */ 
   public boolean placerPion(int r, int x, int y)
   {
      // Si il n'y a pas de pion
      if(pions[r-1][x-1][y-1]==pasPion)
      {
         // On place le pion
         pions[r-1][x-1][y-1] = noJoueur1;
         nbPionAPlacer1--;
         nbPion1++;
         // On dit au serveur qu'on a placer un pion
         envoyer("pla#"+r+"#"+x+"#"+y+"#");
         testCharret(r, x, y);
         return true;
      }
      return false;
   }
   
   /**
    * Test si on a gagné
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   private boolean gagne()
   {
      return nbPion2+nbPionAPlacer2 < 3;
   }
   
   /**
    * Test si on a perdu
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   private boolean perdu()
   {
      return nbPion1+nbPionAPlacer1 < 3;
   }
   
   /**
    * Retourne si le pion est dans un charret
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param r
    * @param x
    * @param y
    * @return
    */ 
   private boolean charret(int r, int x, int y)
   {
      int i;
      for(i=1; i<=3 && pions[r-1][x-1][i-1]==pions[r-1][x-1][y-1]; i++);
      if(i==4)return true;
      for(i=1; i<=3 && pions[r-1][i-1][y-1]==pions[r-1][x-1][y-1]; i++);
      if(i==4)return true;
      if(x==2 || y==2)
      {
         for(i=1; i<=3 && pions[i-1][x-1][y-1]==pions[r-1][x-1][y-1]; i++);
         if(i==4)return true;
      }
      return false;
   }
   
   /**
    * Pour savoir si le joueur peut enlever un pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @return
    */ 
   private boolean peutEnlever()
   {
      for(int r=1; r<=3; r++)
         for(int x=1; x<=3; x++)
            for(int y=1; y<=3; y++)
               if(pions[r-1][x-1][y-1]==noJoueur2 && !charret(r, x, y))
                  return true;
      return false;
   }
   
   /**
    * Pour enlever un pion
    * @author Lucien Chaubert / Louis Jaquier / Thierry Forchelet
    * @param r
    * @param x
    * @param y
    * @return Si l'enlévement est valide
    */ 
   public boolean enleverPion(int r, int x, int y)
   {
      // Si le pion appartient à l'autre joueur
      if(pions[r-1][x-1][y-1]==noJoueur2 && !charret(r, x, y))
      {
         // On enlève le pion
         pions[r-1][x-1][y-1] = pasPion;
         nbPion2--;
         // On dit au serveur qu'on a enlevé un pion
         envoyer("enl#"+r+"#"+x+"#"+y+"#");
         
         // Si on a gagne
         if(gagne())
         {
            vue.obtZone().afficherMessage("");
            vue.msgBox("Vous avez gagné");
         }
         else
         {
            // On dit au serveur qu'on a fini
            vue.obtZone().afficherMessage("L'autre joueur est entraint de jouer...");
            envoyer("ato#");
         }
         return true;
      }
      return false;
   }
   
   public static void main (String args[])
   {
      new Client();
   }
}
